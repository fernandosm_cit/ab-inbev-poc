app.controller('WaterfallController', ['$scope', function ($scope) {

    $scope.welcomeMessage = 'Bem vindo ao Grafico de Cascata - Poc';
    $scope.welcomeMessage2 = 'Bem vindo ao Grafico de Cascata - 2 - Poc';

    $scope.myDataSource = {
        chart: {
            caption: "Age profile of website visitors",
            subcaption: "Last Year",
            startingangle: "120",
            showlabels: "1",
            showlegend: "1",
            enablemultislicing: "1",
            slicingdistance: "15",
            showpercentvalues: "1",
            showpercentintooltip: "1",
            plottooltext: "Age group : $label Total visit : $datavalue"            
        },
        data: [
                {
                    "label": "Online sales",
                    "value": "420000"
                },
                {
                    "label": "Store Sales",
                    "value": "710000"
                },
                {
                    "label": "Total Sales",
                    "issum": "1"
                },
                {
                    "label": "Fixed Costs",
                    "value": "-250000"
                },
                {
                    "label": "Variable Costs",
                    "value": "-156000"
                },
                {
                    "label": "COGS",
                    "value": "-310000"
                },
                {
                    "label": "Promotion Costs",
                    "value": "-86000"
                },
                {
                    "label": "Total Costs",
                    "issum": "1",
                    "cumulative": "0"
                }
        ]
    };


    $scope.myDataSource2 = {
        chart: {
                caption: "Age profile of website visitors",
                subcaption: "Last Year",
                startingangle: "120",
                showlabels: "0",
                showlegend: "1",
                enablemultislicing: "0",
                slicingdistance: "15",
                showpercentvalues: "1",
                showpercentintooltip: "0",
                plottooltext: "Age group : $label Total visit : $datavalue",
                theme: "fint"
        },
        data: [
            {
                label: "Teenage",
                value: "1250400"
            },
            {
                label: "Adult",
                value: "1463300"
            },
            {
                label: "Mid-age",
                value: "1050700"
            },
            {
                label: "Senior",
                value: "491000"
            }
        ]
    };

}]);