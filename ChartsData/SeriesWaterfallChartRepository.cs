﻿using System;
using System.Collections.Generic;
using System.Text;
using Dapper;
using DapperExtensions;
using DapperExtensions.Sql;
using DapperExtensions.Mapper;
using ChartsModel.Waterfall;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Collections;

namespace ChartsData
{
    public class SeriesWaterfallChartRepository
    {
        IDbConnection conn;

        public SeriesWaterfallChartRepository()
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["AbInbevDataConnectionString"].ConnectionString);
        }



        public int Insert(SerieWaterfallChart entity)
        {
            conn.Open();

            int createdId = conn.Insert<SerieWaterfallChart>(entity).Id;

            conn.Close();

            return createdId;
        }

        public void BulkInsert(IList<SerieWaterfallChart> entities)
        {
            conn.Open();

            conn.Insert<SerieWaterfallChart>(entities);

            conn.Close();
        }
    }
}
