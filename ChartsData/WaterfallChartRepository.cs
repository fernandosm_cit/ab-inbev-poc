﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DapperExtensions;
using DapperExtensions.Sql;
using DapperExtensions.Mapper;
using System.Configuration;
using ChartsModel.Waterfall;
using System.Data;

namespace ChartsData
{
    public class WaterfallChartRepository
    {
        IDbConnection conn;

        public WaterfallChartRepository()
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["AbInbevDataConnectionString"].ConnectionString);
        }

        public int Insert(WaterfallChart entity)
        {
            conn.Open();

            int createdId = conn.Insert<WaterfallChart>(entity);

            conn.Close();

            return createdId;
        }

        public WaterfallChart Retrieve(int id)
        {
            conn.Open();

            var query = @"
               SELECT * FROM WaterfallChart WHERE Id = @Id

               SELECT * FROM SerieWaterfallChart WHERE WaterfallChartId = @Id
                ";

            using (var result = conn.QueryMultiple(query, new { Id = id }))
            {
                var supplier = result.Read<WaterfallChart>().SingleOrDefault();
                var products = result.Read<SerieWaterfallChart>().ToList();
            }

            

            conn.Close();

            return null;
        }
    }
}
