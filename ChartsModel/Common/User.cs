﻿using ChartsModel.Enums;

namespace ChartsModel.Common
{
    public class User
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public UserStatusEnum Status { get; set; }
    }
}
