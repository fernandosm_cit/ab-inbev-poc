﻿
namespace ChartsModel.Waterfall
{
    public class SerieWaterfallChart
    {
        public int Id { get; set; }

        public int WaterfallChartId { get; set; }

        public string Name { get; set; }

        public int X { get; set; }

        public int Y { get; set; }

        public bool IsSum { get; set; }

        public bool isIntermediateSum { get; set; }
    }
}
