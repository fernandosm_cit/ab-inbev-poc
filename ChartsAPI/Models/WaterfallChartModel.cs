﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChartsAPI.Models
{
    public class WaterfallChartModel
    {
        public ChartModel Chart { get; set; }

        public TitleModel Title { get; set; }

        public XAxisModel XAxis { get; set; }

        public YAxisModel YAxis { get; set; }

        public LegendModel Legend { get; set; }
    }
}