﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChartsAPI.Models
{
    public class ChartModel
    {
        public string Type { get; set; }
        public bool Inverted { get; set; }
    }
}