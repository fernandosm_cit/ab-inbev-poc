﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ChartsData;
using ChartsModel.Waterfall;

namespace ChartsDataTest
{
    /// <summary>
    /// Summary description for UnitTest2
    /// </summary>
    [TestClass]
    public class UnitTest2
    {
        public UnitTest2()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestMethod1()
        {
            WaterfallChartRepository waterfallRepo = new WaterfallChartRepository();
            SeriesWaterfallChartRepository seriesRepo = new SeriesWaterfallChartRepository();

            var createdId = waterfallRepo.Insert(new ChartsModel.Waterfall.WaterfallChart {  CreatedDate = DateTime.Now, Name = "Waterfall1" });

            IList<SerieWaterfallChart> series = new List<SerieWaterfallChart>();

            for (int i = 0; i < 10; i++)
            {
                series.Add(new SerieWaterfallChart { isIntermediateSum = false, IsSum = false, 
                    Name = string.Format("Serie {0}", i), WaterfallChartId  = createdId, X = 1, Y = 1});
            }

            seriesRepo.BulkInsert(series);
        }

        [TestMethod]
        public void TestMethod2()
        {
            WaterfallChartRepository waterfallRepo = new WaterfallChartRepository();

            waterfallRepo.Retrieve(5);
        }
    }
}
